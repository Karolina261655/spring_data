package com.example.springdata.Service;

import com.example.springdata.Model.Customer;
import com.example.springdata.Model.Order;
import com.example.springdata.Repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Optional;

@Service
public class CustomerService {
    private CustomerRepository customerRepository;

    @Autowired
    public CustomerService(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    public Optional<Customer> getCustomerById(Long id){
        return customerRepository.findById(id);
    }
    public Iterable<Customer> getAllCustomers(){
        return customerRepository.findAll();
    }


    public Customer addCustomer(Customer customer) {
        return customerRepository.save(customer);
    }

    public Customer updateCustomer(Long id, Customer customer) {
        Optional<Customer> customerOptional = customerRepository.findById(id);
        if (customerOptional.isPresent()) {
            customer.setId(id);
            return customerRepository.save(customer);
        }
        return null;
    }

    public Optional<Customer> modifyCustomer(Long id, Map<String, Object> fields){
        Optional<Customer> customerOpt = customerRepository.findById(id);
        if(customerOpt.isPresent()){
            Customer customer = customerOpt.get();
            fields.forEach((key, value) -> {
                switch (key){
                    case "name":
                        customer.setName((String)value);
                        break;
                    case "address":
                        customer.setAddress((String)value);
                        break;
                }
            });
            return Optional.of(customerRepository.save(customer));
        }
        return Optional.empty();
    }


}
