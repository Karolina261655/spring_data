package com.example.springdata.Service;

import com.example.springdata.Model.Product;
import com.example.springdata.Repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Optional;

@Service
public class ProductService {
    private ProductRepository productRepository;

    @Autowired
    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public Optional<Product> getProductById(Long id){
        return productRepository.findById(id);
    }

    public Iterable<Product> getAllProducts(){
        return productRepository.findAll();
    }

    public Product addProduct(Product product) {
        return productRepository.save(product);
    }

    public Product updateProduct(Long id, Product product) {
        Optional<Product> productOptional = productRepository.findById(id);
        if (productOptional.isPresent()) {
            product.setId(id);
            return productRepository.save(product);
        }
        return null;
    }

    public Optional<Product> modifyProduct(Long id, Map<String, Object> fields){
        Optional<Product> productOpt = productRepository.findById(id);
        if(productOpt.isPresent()){
            Product product = productOpt.get();
            fields.forEach((key, value) -> {
                switch (key){
                    case "name":
                        product.setName((String)value);
                        break;
                    case "price":
                        product.setPrice(((Double) value).floatValue());
                        break;
                    case "available":
                        product.setAvailable((Boolean)value);
                        break;
                }
            });
            return Optional.of(productRepository.save(product));
        }
        return Optional.empty();
    }
}
