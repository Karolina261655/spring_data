package com.example.springdata.Service;

import com.example.springdata.Model.Customer;
import com.example.springdata.Model.Order;
import com.example.springdata.Model.Product;
import com.example.springdata.Repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

@Service
public class OrderService {
    private OrderRepository orderRepository;

    @Autowired
    public OrderService(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }
    public Optional<Order> getOrderById(Long id){
        return orderRepository.findById(id);
    }
    public Iterable<Order> getAllOrders(){
        return orderRepository.findAll();
    }

    public Order addOrder(Order order) {
        return orderRepository.save(order);
    }

    public Order updateOrder(Long id, Order order) {
        Optional<Order> orderOptional = orderRepository.findById(id);
        if (orderOptional.isPresent()) {
            order.setId(id);
            return orderRepository.save(order);
        }
        return null;
    }

    public Optional<Order> modifyOrder(Long id, Map<String, Object> fields){
        Optional<Order> orderOptional = orderRepository.findById(id);
        if(orderOptional.isPresent()){
            Order order = orderOptional.get();
            fields.forEach((key, value) -> {
                switch (key){
                    case "customer":
                        order.setCustomer((Customer) value);
                        break;
                    case "products":
                        order.setProducts((Set<Product>)value);
                        break;
                    case "placeDate":
                        order.setPlaceDate((LocalDateTime)value);
                        break;
                    case "status":
                        order.setStatus((String) value);
                        break;
                }
            });
            return Optional.of(orderRepository.save(order));
        }
        return Optional.empty();
    }

}
