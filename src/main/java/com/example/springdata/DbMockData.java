//package com.example.springdata;
//
//import com.example.springdata.Model.Customer;
//import com.example.springdata.Model.Order;
//import com.example.springdata.Model.Product;
//import com.example.springdata.Model.User;
//import com.example.springdata.Repository.CustomerRepository;
//import com.example.springdata.Repository.OrderRepository;
//import com.example.springdata.Repository.ProductRepository;
//import com.example.springdata.Repository.UserRepository;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.context.event.ApplicationReadyEvent;
//import org.springframework.context.event.EventListener;
//import org.springframework.security.crypto.password.PasswordEncoder;
//import org.springframework.stereotype.Component;
//
//import java.time.LocalDateTime;
//import java.util.HashSet;
//import java.util.Set;
//
//@Component
//
//public class DbMockData {
//    private ProductRepository productRepository;
//    private OrderRepository orderRepository;
//    private CustomerRepository customerRepository;
//    private UserRepository userRepository;
//    private PasswordEncoder passwordEncoder;
//    @Autowired
//    public DbMockData(ProductRepository productRepository, OrderRepository orderRepository, CustomerRepository customerRepository, UserRepository userRepository, PasswordEncoder passwordEncoder) {
//        this.productRepository = productRepository;
//        this.orderRepository = orderRepository;
//        this.customerRepository = customerRepository;
//        this.userRepository = userRepository;
//        this.passwordEncoder = passwordEncoder;
//    }
//
//    @EventListener(ApplicationReadyEvent.class)
//    public void fill() {
//        Product product = new Product("Bubble tea", 9.99f, true);
//        Product product1 = new Product("Matcha Latte", 8.99f, true);
//        Customer customer = new Customer("Jan Kowalski", "Wrocław");
//        Set<Product> products = new HashSet<>() {
//            {
//                add(product);
//                add(product1);
//            }
//        };
//        Order order = new Order(customer, products, LocalDateTime.now(), "in progress");
//        User user1 = new User(1,"admin",passwordEncoder.encode("password"),"ADMIN");
//        User user2 = new User(2,"customer",passwordEncoder.encode("password"),"CUSTOMER");
//        productRepository.save(product);
//        productRepository.save(product1);
//        customerRepository.save(customer);
//        orderRepository.save(order);
//        userRepository.save(user1);
//        userRepository.save(user2);
//    }
//}