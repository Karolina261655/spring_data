//package com.example.springdata;
//
//import com.example.springdata.Model.Product;
//import com.example.springdata.Repository.ProductRepository;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.context.event.ApplicationReadyEvent;
//import org.springframework.context.event.EventListener;
//import org.springframework.stereotype.Component;
//
//@Component
//public class Start {
//
//    private ProductRepository productRepo;
//
//    @Autowired
//    public Start(ProductRepository productRepo) {
//        this.productRepo = productRepo;
//    }
//
//    @EventListener(ApplicationReadyEvent.class)
//    public void run(){
//        Product product1 = new Product("Czekolada", 3.99f, true);
//        productRepo.save(product1);
//
//        Product product2 = new Product("Sok pomarańczowy", 4.99f, false);
//        productRepo.save(product2);
//
//        Product product3 = new Product("Ciastka", 5.40f, true);
//        productRepo.save(product3);
//
//
// }
//}
