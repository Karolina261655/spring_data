package com.example.springdata.Controller;

import com.example.springdata.Model.Customer;
import com.example.springdata.Model.Order;
import com.example.springdata.Model.Product;
import com.example.springdata.Service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

@RestController
@RequestMapping("/api")
public class OrderController {
    private OrderService orderService;

    @Autowired
    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @GetMapping("/order/{id}")
    public Optional<Order> getById(@PathVariable Long id){
        return orderService.getOrderById(id);
    }

    @GetMapping("/order/all")
    public Iterable<Order> getAll(){
        return orderService.getAllOrders();
    }

    @PostMapping("/order")
    public Order add(@RequestBody Order order){
        return orderService.addOrder(order);
    }

    @PutMapping("/admin/order/{id}")
    public Order update(@PathVariable Long id, @RequestBody Order order){
        return orderService.updateOrder(id, order);
    }

    @PatchMapping("/admin/order/{id}")
    public Optional<Order> modify(@PathVariable Long id, @RequestBody Map<String, Object> fields){
        return orderService.modifyOrder(id, fields);
    }

}