package com.example.springdata.Controller;

import com.example.springdata.Model.Product;
import com.example.springdata.Service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class ProductController {
    private ProductService productService;

    @Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("/product/{id}")
    public Optional<Product> getById(@PathVariable Long id){
        return productService.getProductById(id);
    }

    @GetMapping("/product/all")
    public Iterable<Product> getAll(){
        return productService.getAllProducts();
    }

    @PostMapping("/admin/product")
    public Product add(@RequestBody Product product){
        return productService.addProduct(product);
    }

    @PutMapping("admin/product/{id}")
    public Product update(@PathVariable Long id, @RequestBody Product product){
        return productService.updateProduct(id,product);
    }

    @PatchMapping("/admin/product/{id}")
    public Optional<Product> modify(@PathVariable Long id, @RequestBody Map<String, Object> fields){
        return productService.modifyProduct(id, fields);
    }
}
