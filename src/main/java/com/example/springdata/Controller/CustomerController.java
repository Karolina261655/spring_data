package com.example.springdata.Controller;

import com.example.springdata.Model.Customer;
import com.example.springdata.Model.Order;
import com.example.springdata.Service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class CustomerController {
    private CustomerService customerService;

    @Autowired
    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping("/customer/{id}")
    public Optional<Customer> getById(@PathVariable Long id){
        return customerService.getCustomerById(id);
    }

    @GetMapping("/customer/all")
    public Iterable<Customer> getAll(){
        return customerService.getAllCustomers();
    }

    @PostMapping("/admin/customer")
    public Customer add(@RequestBody Customer customer){
        return customerService.addCustomer(customer);
    }
    @PutMapping("/admin/customer/{id}")
    public Customer update(@PathVariable Long id, @RequestBody Customer customer){
        return customerService.updateCustomer(id, customer);
    }

    @PatchMapping("/admin/customer/{id}")
    public Optional<Customer> modify(@PathVariable Long id, @RequestBody Map<String, Object> fields){
        return customerService.modifyCustomer(id, fields);
    }

}
