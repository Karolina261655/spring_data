GET http://localhost:8080/api/product/1

GET http://localhost:8080/api/product/all

POST http://localhost:8080/api/admin/product
{
        "id": 3,
        "name": "Woda",
        "price": 2.99,
        "available": true
}

PUT http://localhost:8080/api/admin/product/1
{
    "name": "Bubble tea",
    "price": 14.99,
    "available": true
}

PATCH http://localhost:8080/api/admin/product/1
{
    "available": false
}

GET http://localhost:8080/api/customer/1

GET http://localhost:8080/api/customer/all

POST http://localhost:8080/api/admin/customer
{
    "id": 2,
    "name": "Anna Kowalska",
    "address": "Warszawa"
}

PUT http://localhost:8080/api/admin/customer/2
{
    "name": "Anna Kowalska",
    "address": "Wrocław"
}

PATCH http://localhost:8080/api/admin/customer/2
{
    "address": "Warszawa"
}

GET http://localhost:8080/api/order/1

GET http://localhost:8080/api/order/all

POST http://localhost:8080/api/order
{
        "id": 2,
        "customer": {
            "id": 1,
            "name": "Jan Kowalski",
            "address": "Wrocław"
        },
        "products": [
            {
                "id": 3,
                "name": "Czekolada",
                "price": 3.99,
                "available": true
            },
            {
                "id": 4,
                "name": "Sok pomarańczowy",
                "price": 4.99,
                "available": false
            }
        ],
        "placeDate": "2023-04-28T20:53:05.136586",
        "status": "done"
    }

PUT http://localhost:8080/api/admin/order/2
{
        "id": 2,
        "customer": {
            "id": 1,
            "name": "Jan Kowalski",
            "address": "Wrocław"
        },
        "products": [
            {
                "id": 3,
                "name": "Czekolada",
                "price": 3.99,
                "available": true
            },
            {
                "id": 5,
                "name": "Ciastka",
                "price": 5.4,
                "available": true
            }
        ],
        "placeDate": "2023-04-28T20:53:05.136586",
        "status": "in progress"
    }

PATCH http://localhost:8080/api/admin/order/2
{
    "status": "done"
}

